package org.example;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;



public class CalculatorTest {
    @Test
    public void factorial_inputOne_returnOne(){
        Calculator calculator = new Calculator();
        long n = 1;
        long expectResult = 1;

        long result = calculator.factorial(n);

        Assertions.assertEquals(expectResult, result);


    }
}