package org.example;

import java.util.Scanner;

public class SimpleCalc {
    public static void main(String[] args) {
        char operator;
        Double number1, number2;

        Scanner input = new Scanner(System.in);

        System.out.println("Choose an operator: +, -, *, or /");
        operator = input.next().charAt(0);

        System.out.println("Enter first number");
        number1 = input.nextDouble();

        System.out.println("Enter second number");
        number2 = input.nextDouble();

        switch (operator) {
            case '+':
                add(number1, number2);
                break;
            case '-':
                subtract(number1, number2);
                break;
            case '*':
                multiply(number1, number2);
                break;
            case '/':
                divide(number1, number2);
                break;
            default:
                System.out.println("Invalid operator!");
                break;
        }
    }

    public static double add(double number1, double number2) {
        double result = number1 + number2;
        System.out.println(number1 + " + " + number2 + " = " + result);
        return result;
    }

    public static double subtract(double number1, double number2) {
        double result = number1 - number2;
        System.out.println(number1 + " - " + number2 + " = " + result);
        return result;
    }

    public static double multiply(double number1, double number2) {
        double result = number1 * number2;
        System.out.println(number1 + " * " + number2 + " = " + result);
        return result;
    }

    public static double divide(double number1, double number2) {
        double result = 0;
        if (number2 == 0) {
            System.out.println("Can't be divided by zero");
        } else {
            result = number1 / number2;
            System.out.println(number1 + " / " + number2 + " = " + result);
        }
        return result;
    }

}