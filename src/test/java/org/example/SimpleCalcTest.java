package org.example;

import org.junit.jupiter.api.Test;
import static  org.junit.jupiter.api.Assertions.*;


public class SimpleCalcTest {
    @Test
    public void testAdd() {
        assertEquals(15, SimpleCalc.add(14, 1));
    }
    @Test
    public void testSubtract() {
        assertEquals(10, SimpleCalc.subtract(12, 2));
    }
    @Test
    public void  testMultiply() {
        assertEquals(12, SimpleCalc.multiply(3, 4));
    }
    @Test
    public void testDivide(){
        assertEquals(6, SimpleCalc.divide(6, 1));
    }
    @Test
    public void testDivideByZero(){
        double result = SimpleCalc.divide(2, 0);
        assertEquals(0, result);
    }
}